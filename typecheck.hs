module TypeCheck where

import Control.Monad.State
import Data.List
import qualified Data.HashMap.Lazy as HashMap
import Data.Maybe

import Parser

type ClassHierarchy = HashMap.HashMap String ClassTypes

type MethodType = (String, [String])

type ClassTypes = (String, String, [String], HashMap.HashMap String MethodType, HashMap.HashMap String VarType)

type VarType = String

className :: ClassTypes -> String
className (name, _, _, _, _) = name

buildHierarchy :: Program -> ClassHierarchy
buildHierarchy (Program classes _) =
  let l = map classTypes classes
  in HashMap.fromListWith twoClassesError (built_in_hierarchy ++ l)

built_in_hierarchy :: [(String, ClassTypes)]
built_in_hierarchy = [
  ("Obj", ("Obj", "$Top", [], HashMap.fromList [("EQUALS", ("Bool", ["Obj"])),
                                                ("STR", ("String", [])),
                                                ("PRINT", ("Nothing", []))],
                              HashMap.empty)),
  ("Bool", ("Bool", "Obj", [], HashMap.fromList [("EQUALS", ("Bool", ["Obj"])),
                                                 ("STR", ("String", []))],
                               HashMap.empty)),
  ("String", ("String", "Obj", [], HashMap.fromList [("EQUALS", ("Bool", ["Obj"])),
                                                     ("STR", ("String", [])),
                                                     ("PLUS", ("String", ["String"]))],
                                   HashMap.empty)),
  ("Int", ("Int", "Obj", [], HashMap.fromList [("EQUALS", ("Bool", ["Obj"])),
                                               ("STR", ("String", [])),
                                               ("PLUS", ("Int", ["Int"])),
                                               ("MINUS", ("Int", ["Int"])),
                                               ("TIMES", ("Int", ["Int"])),
                                               ("DIVIDE", ("Int", ["Int"])) ],
                             HashMap.empty )),
  ("Nothing", ("Nothing", "Obj", [], HashMap.fromList [("EQUALS", ("Bool", ["Obj"])),
                                                       ("STR", ("String", []))],
               HashMap.empty)),
  ("$Main", ("$Main", "Obj", [], HashMap.fromList [("Main", ("Nothing", []))], HashMap.empty))
  
  ]

classTypes :: ClassDef -> (String, ClassTypes)
classTypes (ClassDef name args parent statements methods) =
  (name, (name, parent, map argType args, buildMethodTable name methods, buildVarTable statements))

twoClassesError c _ = error $ "Error: two or more classes named \"" ++ className c ++ "\""

buildMethodTable :: String -> [MethodDef] -> HashMap.HashMap String MethodType
buildMethodTable className methods =
  let l = map methodType methods
  in HashMap.fromListWith (twoMethodsError className) l

methodType :: MethodDef -> (String, MethodType)
methodType (MethodDef name args returnType _) = (name, (returnType, map argType args))

twoMethodsError className (name, _) _ = error $ "Error: two or more methods in class \"" ++ className ++ "\" named \"" ++ name ++ "\""

buildVarTable :: [Statement] -> HashMap.HashMap String VarType
buildVarTable = HashMap.fromList . mapMaybe varType

varType :: Statement -> Maybe (String, VarType)
varType (Assign (Just (Var o)) name bot _) = if o == "this" then Just (name, bot) else Nothing
varType _ = Nothing

argName :: FormalArg -> String
argName (FormalArg name _) = name
                                 
argType :: FormalArg -> String
argType (FormalArg _ t) = t

toMainClass :: [Statement] -> ClassDef
toMainClass s = ClassDef "$Main" [] "Obj" [] [MethodDef "$Main" [] "Nothing" s]

checkProgram :: Program -> State ClassHierarchy ()
checkProgram (Program classes main) = mapM_ checkClass (toMainClass main : classes)

checkClass :: ClassDef -> State ClassHierarchy ()
checkClass c = do
  checkHierarchy
  initialiseCheck c
  classTypeCheck c

checkHierarchy :: State ClassHierarchy ()
checkHierarchy = do
  classes <- gets HashMap.keys
  mapM_ (checkForLoop HashMap.empty) classes

checkForLoop :: (HashMap.HashMap String ()) -> String -> State ClassHierarchy ()
checkForLoop children c = if c == "$Top"
  then return ()
  else do
    if HashMap.member c children then error $ "Error: class '" ++ c ++ "' inherits from itself."
                                 else return ()
    (_, parent, _, _, _) <- gets $
      HashMap.lookupDefault (error $ "Error: parent class '" ++ c ++ "' undefined.") c
    checkForLoop (HashMap.insert c () children) parent
  

initialiseCheck :: ClassDef -> State ClassHierarchy ()
initialiseCheck (ClassDef name args parent constructor methods)  = do
  members <- getMembers name
  conErrors <- return $ checkInitConstructor members (map argName args) constructor
  methodErrors <- return $ concat $ map (checkInitMethod members) methods
  totalErrors <- return $ conErrors ++ methodErrors
  case totalErrors of
    [] -> return ()
    _ -> error $ unlines $ map (++ (" in class '" ++ name ++ "'.")) totalErrors

getMembers :: String -> State ClassHierarchy [String]
getMembers c = do
  (_, parent, _, _, memberMap) <- gets $ (HashMap.! c)
  inheritedMembers <- if parent == "$Top" then return []
                                          else getMembers parent
  return $ inheritedMembers ++ (HashMap.keys memberMap)

checkInitConstructor :: [String] -> [String] -> [Statement] -> [String]
checkInitConstructor members args body = let
  unqualifiedMembers = members \\ args
  qualifiedMembers = map ("this." ++) members
  startingMap = HashMap.fromList $ map (\x->(x,())) args
  computation = do
    ref_errs <- checkInitStatements body
    init_errs <- concat <$> mapM checkInitialised members
    return $ ref_errs ++ init_errs
  in evalState computation startingMap

checkInitialised :: String -> State (HashMap.HashMap String ()) [String]
checkInitialised m = do
  is_initialised <- gets $ HashMap.member $ "this." ++ m
  if is_initialised then return []
                    else return ["Class member " ++ m ++ " was not initalised in the constructor"]

checkInitMethod :: [String] -> MethodDef -> [String]
checkInitMethod members (MethodDef name args _ body) = let
  argNames = map argName args
  unqualifiedMembers = members \\ argNames
  qualifiedMembers = map ("this." ++) members
  startingMap = HashMap.fromList $ map (\x->(x,())) $ argNames ++ unqualifiedMembers ++ qualifiedMembers
  computation = map (++ " in method " ++ name) <$> checkInitStatements body
  in evalState computation startingMap

checkInitStatements :: [Statement] -> State (HashMap.HashMap String ()) [String]
checkInitStatements ss = concat <$> mapM checkInitStatement (reverse ss)

checkInitStatement :: Statement -> State (HashMap.HashMap String ()) [String]
checkInitStatement (Assign Nothing x _ right) = do
  errs <- checkInitExpr right
  unqualified <- gets $ HashMap.member x
  if unqualified then return ()
                 else modify $ HashMap.insert ("this." ++ x) ()
  return errs
checkInitStatement (Assign (Just (Var "this")) x _ right) = do
  errs <- checkInitExpr right
  modify $ HashMap.insert ("this." ++ x) ()
  return errs
checkInitStatement (Assign (Just left) x c right) = do
  lerr <- checkInitExpr left
  case lerr of
    e : t -> return lerr
    [] -> checkInitExpr right
checkInitStatement (IfBlock cond thenblock elseblock) = do
  cerr <- checkInitExpr cond
  case cerr of
    e : t -> return $ map (++ " in conditional of if") cerr
    [] -> do
      currentState <- get
      lerr <- checkInitStatements thenblock
      optionThen <- get
      put currentState
      rerr <- checkInitStatements elseblock
      optionElse <- get
      put $ HashMap.intersection optionThen optionElse
      return $ map (++ " in thenblock of an if") lerr ++ map (++ " in elseblock of an if") rerr
checkInitStatement (WhileBlock cond body) = do
  cerr <- checkInitExpr cond
  case cerr of
    _:_ -> return $ map (++ " in conditional of a while block") cerr
    [] -> do
      currentState <- get
      err <- checkInitStatements body
      put currentState
      return $ map (++ " in body of a while loop") err
checkInitStatement (ReturnStatement exp) = checkInitExpr exp
checkInitStatement (Expression exp) = checkInitExpr exp
checkInitStatement (TypeCase exp typealts) = do
  e_err <- checkInitExpr exp
  case e_err of
    _:_ -> return $ map (++ " in typecase expression") e_err
    [] -> concat <$> mapM checkInitTypeAlt typealts

checkInitTypeAlt :: TypeAlt -> State (HashMap.HashMap String ()) [String]
checkInitTypeAlt (TypeAlt var _ body) = do
  currentState <- get
  modify $ HashMap.insert var ()
  err <- checkInitStatements body
  put currentState
  return $ map (++ " in typecase block") err

checkInitExpr :: Expr -> State (HashMap.HashMap String ()) [String]
checkInitExpr (Var x) = do
  unqualified <- gets $ HashMap.member x
  if unqualified then return []
                 else checkInitExpr $ Access (Var "this") x
checkInitExpr (Access (Var "this") x) = do
  initialised <- gets $ HashMap.member $ "this." ++ x
  if initialised then return []
                 else return ["Error: variable " ++ x ++ " used before initalisation"]
checkInitExpr (Access exp x) = checkInitExpr exp
checkInitExpr (IntLiteral _) = return []
checkInitExpr (StrLiteral _) = return []
checkInitExpr (MethodCall exp name args) = do
  oerr <- checkInitExpr exp
  aerr <- concat <$> mapM checkInitExpr args
  return $ oerr ++ map (++ (" in arguments to method '" ++ name ++ "'")) aerr
checkInitExpr (Constructor t args) = do
  aerr <- concat <$> mapM checkInitExpr args
  return $ map (++ (" in arguments to constructor '" ++ t ++ "'")) aerr
checkInitExpr (AndExp l r) = (++) <$> checkInitExpr l <*> checkInitExpr r
checkInitExpr (OrExp l r) = (++) <$> checkInitExpr l <*> checkInitExpr r
checkInitExpr (NotExp b) = checkInitExpr b
  
classTypeCheck :: ClassDef -> State ClassHierarchy ()
classTypeCheck c = return ()

typeCheck :: Program -> ClassHierarchy
typeCheck p = let initialHierarchy = buildHierarchy p
                  finalState = checkProgram p
              in execState finalState initialHierarchy
