# CIS-561-project
Here is what I have done to make this run:

1) Install "Haskell Platform" from https://www.haskell.org/downloads

2) Install Alex with the cmd/Terminal command "cabal install alex"

3) Install Happy with the cmd/Terminal command "cabal install happy"

4) Build lexer with the command "alex scanner.x", with cis-561-project/ as the working directory.

5) Then build parser similarly, with the command "happy parser.y".

6) Run the compiler using "runHaskell compile.hs [qk file] [c file]"

This compiles all statements only to "return 0;", but it does automatically generate all needed class method lookup tables and object structs.

