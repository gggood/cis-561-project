module Compile where

import Parser
import TypeCheck

import Control.Monad.State
import Data.List
import Data.Maybe
import qualified Data.HashMap.Lazy as HashMap

data Context = Context { nameMap :: HashMap.HashMap String Int
                       , varMap :: HashMap.HashMap String String
                       }

toCType :: String -> String
toCType "$Bot" = "void*"
toCType t = "struct obj_" ++ t ++ "*"

getNewName :: String -> State Context String
getNewName name = do
  lastIndex <- gets $ HashMap.lookup name . nameMap
  x <- return $ case lastIndex of
    Nothing -> 0
    Just n -> n + 1
  modify $ \c -> c { nameMap = HashMap.insert name x (nameMap c) }
  return $ name ++ show x

compile :: Program -> ClassHierarchy -> [String]
compile (Program classes main) ch =
  let inclusion = ["#include <stdlib.h>", "#include <string.h>", "#include <stdio.h>"]
      realHierarchy = HashMap.delete "$Main" ch
      forwardDeclarations = classDeclarations realHierarchy
      structDefinitions = concat $ HashMap.elems $ fmap (classStructDefinition realHierarchy) realHierarchy
      objDefinitions = concat $ HashMap.elems $ fmap (objStructDefinition realHierarchy) realHierarchy
      mainFunction = compileMain realHierarchy main
      classDefinitions = concat $ map (compileClass realHierarchy) classes
      noneDef = ["struct obj_Nothing* var_none;"]
  in inclusion ++ forwardDeclarations ++ structDefinitions ++ objDefinitions ++ noneDef ++ builtIn ++ classDefinitions ++ mainFunction

classDeclarations :: ClassHierarchy -> [String]
classDeclarations ch =
  let classDecs = concat $ map oneClassDecl (HashMap.keys ch)
      methodDecs = concat $ HashMap.elems $ fmap generateMethodDecs ch
  in classDecs ++ methodDecs

oneClassDecl :: String -> [String]
oneClassDecl c = [ "struct class_struct_" ++ c ++ ";"
                 , "struct class_struct_" ++ c ++ " class_" ++ c ++ ";"
                 , "struct obj_" ++ c ++ ";"
                 ]

generateMethodDecs :: ClassTypes -> [String]
generateMethodDecs (name, parent, argTypes, methodTypes, members) =
  let constructor = declareConstructor name argTypes
      methods = HashMap.elems $ HashMap.mapWithKey (declareMethod name) methodTypes
  in constructor : methods

declareConstructor :: String -> [String] -> String
declareConstructor name argTypes =
  let argDec = concat $ intersperse ", " $ map toCType argTypes
  in toCType name ++ " constructor_" ++ name ++ "(" ++ argDec ++ ");"

declareMethod :: String -> String -> MethodType -> String
declareMethod className mName (returnType, argTypes) =
  let argDec = concat $ intersperse ", " $ map toCType (className : argTypes)
      realName = "method_" ++ className ++ "_" ++ mName
  in toCType returnType ++ " " ++ realName ++ "(" ++ argDec ++ ");"

classStructDefinition :: ClassHierarchy -> ClassTypes -> [String]
classStructDefinition ch c@(name, parent, argTypes, methodMap, varMap) =
  let argDec = concat $ intersperse ", " $ map toCType argTypes
      classStart = [ "struct class_struct_" ++ name ++ " {"
                   , "  void* super;"
                   , "  " ++ toCType name ++ " (*constructor)(" ++ argDec ++ ");"
                   ]
      classEnd = ["};"]
      classFuns = evalState (classFunctions ch c name) (fmap Just methodMap)
  in classStart ++ classFuns ++ classEnd

classFunction :: ClassHierarchy -> String -> (String, MethodType) -> State (HashMap.HashMap String (Maybe MethodType)) (Maybe String)
classFunction ch cName (mName, (returnType, argTypes)) = do
  override <- gets $ HashMap.lookup mName
  case override of
    Just (Just (realReturn, realArgs)) -> do
      modify $ HashMap.insert mName Nothing
      return $ Just $ mPointer cName mName realReturn realArgs
    Just (Nothing) -> return Nothing
    Nothing -> do
      modify $ HashMap.insert mName Nothing
      return $ Just $ mPointer cName mName returnType argTypes

mPointer :: String -> String -> String -> [String] -> String
mPointer cName mName returnType argTypes =
  let decArgs = concat $ intersperse ", " $ map toCType (cName : argTypes)
  in "  " ++ toCType returnType ++ " (*method_" ++ mName ++ ")(" ++ decArgs ++ ");"

classFunctions :: ClassHierarchy -> ClassTypes -> String -> State (HashMap.HashMap String (Maybe MethodType)) [String]
classFunctions ch (name, parent, args, methodMap, varMap) realName = do
  inheritedFunctions <- case HashMap.lookup parent ch of
    Nothing -> return []
    Just parentTypes -> classFunctions ch parentTypes realName
    -- this is to make sure we inherit all the methods from our ancestors in the right order
  currentFunctions <- fmap catMaybes $ mapM (classFunction ch realName) $ HashMap.toList methodMap
  return $ inheritedFunctions ++ currentFunctions

objStructDefinition :: ClassHierarchy -> ClassTypes -> [String]
objStructDefinition ch c@(name, parent, args, methodMap, memberMap) =
  let start = [ "struct obj_" ++ name ++ " {"
              , "  void* clazz;" ]
      memberComp = classMembers ch c
      members = evalState memberComp HashMap.empty
      end = ["};"]
  in start ++ members ++ end

classMembers :: ClassHierarchy -> ClassTypes -> State (HashMap.HashMap String ()) [String]
classMembers _ ("Obj", _, _, _, _) = return []
classMembers _ ("Bool", _, _, _, _) = return ["  char val;"]
classMembers _ ("Int", _, _, _, _) = return ["  int val;"]
classMembers _ ("Nothing", _, _, _, _) = return []
classMembers _ ("String", _, _, _, _) = return [ "  char* data;"]
classMembers ch c@(name, parent, args, methodMap, memberMap) = do
  inheritedMembers <- case HashMap.lookup parent ch of
    Nothing -> return []
    Just p -> classMembers ch p
  newMembers <- catMaybes <$> mapM classMember (HashMap.toList memberMap)
  return $ inheritedMembers ++ newMembers

classMember :: (String, VarType) -> State (HashMap.HashMap String ()) (Maybe String)
classMember (name, t) = do
  alreadyDone <- gets $ HashMap.member name
  modify $ HashMap.insert name ()
  return $ if alreadyDone then Nothing
                          else Just $ "  " ++ toCType t ++ " member_" ++ name ++ ";"

compileMain :: ClassHierarchy -> [Statement] -> [String]
compileMain ch body =
  let start = ["int main() {"]
      initialise = initialiseStructs ch
      mainCode = compileBody ch [] body
      end = [ "  return 0;"
            , "}"
            ]
  in start ++ initialise ++ mainCode ++ end

initialiseStructs :: ClassHierarchy -> [String]
initialiseStructs ch =
  let classes = HashMap.elems ch
      normalInit = concat $ map (initialiseStruct ch) classes
      noneDef = [ "  var_none = malloc(sizeof(struct obj_Nothing));"
                , "  var_none->clazz = (void*)&class_Nothing;" ]
  in normalInit ++ noneDef

initialiseStruct :: ClassHierarchy -> ClassTypes -> [String]
initialiseStruct ch c@(name, parent, args, methodMap, memberMap) =
  let super = if parent == "$Top" then "0"
                                  else "&class_" ++ parent
      initStatics = [ "super = (void*)" ++ super ++ ";"
                    , "constructor = &constructor_" ++ name ++ ";"
                    ]
      methods = initMethods ch c
  in map (("  class_" ++ name ++ ".") ++) (initStatics ++ methods)

initMethods :: ClassHierarchy -> ClassTypes -> [String]
initMethods ch c@(name, parent, args, methodMap, memberMap) =
  let parentMethods = case HashMap.lookup parent ch of
        Nothing -> []
        Just p -> (initMethods ch p)
      methodInit mName = "method_" ++ mName ++ " = &method_" ++ name ++ "_" ++ mName ++ ";"
      currentMethods = map methodInit (HashMap.keys methodMap)
  in parentMethods ++ currentMethods

compileBody :: ClassHierarchy -> [FormalArg] -> [Statement] -> [String]
compileBody ch args body =
  let pairArgs = map (\a -> (formalArgName a, formalArgType a)) args
      c = Context { nameMap = HashMap.empty
                  , varMap = HashMap.fromList pairArgs
                  }
      textComputation = mapM (compileStatement ch) (reverse body)
      text = evalState textComputation c
  in map ("  " ++) (concat text)

compileClass :: ClassHierarchy -> ClassDef -> [String]
compileClass ch c =
  let argDec arg = toCType (formalArgType arg) ++ " var_" ++ formalArgName arg
      argDecs = concat $ intersperse ", " $ map argDec (c_args c)
      constructorDec = toCType (c_name c) ++ " constructor_" ++ (c_name c) ++ "(" ++ argDecs ++ ") {"
      constructorDef = compileBody ch (c_args c) (c_body c)
      constructor = constructorDec : constructorDef ++ ["}"]
      mArgDecs m = concat $ intersperse ", " $ (toCType (c_name c) ++ " this") : map argDec (args m)
      methodDec m = toCType (returnType m) ++ " method_" ++ c_name c ++ "_" ++ name m ++ "(" ++ mArgDecs m ++ ") {"
      mDef m = compileBody ch (args m) (body m)
      method m = [methodDec m] ++ mDef m ++ ["}"]
      methods = concat $ map method (c_methods c)
  in constructor ++ methods

compileStatement :: ClassHierarchy -> Statement -> State Context [String]
compileStatement ch _ = return ["return 0;"]

builtIn :: [String]
builtIn =
  [ "struct obj_Obj* constructor_Obj() {"
  , "  struct obj_Obj* this = malloc(sizeof(struct obj_Obj));"
  , "  this->clazz = (void*)&class_Obj;"
  , "  return this;"
  , "}"
  , "struct obj_Nothing* method_Obj_PRINT(struct obj_Obj* this) {"
  , "  struct obj_String* str = ((struct class_struct_Obj*)this->clazz)->method_STR(this);"
  , "  printf(str->data);"
  , "  return var_none;"
  , "}"
  , "struct obj_String* method_Obj_STR(struct obj_Obj* this) {"
  , "  struct obj_String* str = constructor_String();"
  , "  str->data = malloc(7*sizeof(char));"
  , "  strcpy(str->data, \"Object\");"
  , "  return str;"
  , "}"
  , "struct obj_Bool* method_Obj_EQUALS(struct obj_Obj* this, struct obj_Obj* other) {"
  , "  struct obj_Bool* b = constructor_Bool();"
  , "  b->val = (this == other);"
  , "  return b;"
  , "}"
  , "struct obj_Bool* constructor_Bool() {"
  , "  struct obj_Bool* this = malloc(sizeof(struct obj_Bool));"
  , "  this->clazz = (void*)&class_Bool;"
  , "  return this;"
  , "}"
  , "struct obj_Bool* method_Bool_EQUALS(struct obj_Bool* this, struct obj_Obj* other) {"
  , "  struct obj_Bool* eq = constructor_Bool();"
  , "  if (other->clazz == (void*)&class_Bool) {"
  , "    eq->val = (this->val == ((struct obj_Bool*)other)->val);"
  , "  } else {"
  , "    eq->val = 0;"
  , "  }"
  , "  return eq;"
  , "}"
  , "struct obj_String* method_Bool_STR(struct obj_Bool* this) {"
  , "  struct obj_String* str = constructor_String();"
  , "  if (this->val) {"
  , "    str->data = malloc(5*sizeof(char));"
  , "    strcpy(str->data, \"True\");"
  , "  } else {"
  , "    str->data = malloc(6*sizeof(char));"
  , "    strcpy(str->data, \"False\");"
  , "  }"
  , "  return str;"
  , "}"
  , "struct obj_Int* constructor_Int() {"
  , "  struct obj_Int* this = malloc(sizeof(struct obj_Int));"
  , "  this->clazz = (void*)&class_Int;"
  , "  return this;"
  , "}"
  , "struct obj_Int* method_Int_MINUS(struct obj_Int* this, struct obj_Int* other) {"
  , "  struct obj_Int* newInt = constructor_Int();"
  , "  newInt->val = this->val - other->val;"
  , "  return newInt;"
  , "}"
  , "struct obj_Int* method_Int_DIVIDE(struct obj_Int* this, struct obj_Int* other) {"
  , "  struct obj_Int* newInt = constructor_Int();"
  , "  newInt->val = this->val / other->val;"
  , "  return newInt;"
  , "}"
  , "struct obj_Bool* method_Int_EQUALS(struct obj_Int* this, struct obj_Obj* other) {"
  , "  struct obj_Bool* eq = constructor_Bool();"
  , "  if (other->clazz == (void*)&class_Int) {"
  , "    eq->val = (this->val == ((struct obj_Int*) other)->val);"
  , "  } else {"
  , "    eq->val = 0;"
  , "  }"
  , "  return eq;"
  , "}"
  , "struct obj_String* method_Int_STR(struct obj_Int* this) {"
  , "  struct obj_String* str = constructor_String();"
  , "  int length = snprintf(NULL, 0, \"%d\", this->val);"
  , "  str->data = malloc((length + 1)*sizeof(char));"
  , "  snprintf(str->data, length + 1, \"%d\", this->val);"
  , "  return str;"
  , "}"
  , "struct obj_Int* method_Int_PLUS(struct obj_Int* this, struct obj_Int* other) {"
  , "  struct obj_Int* newInt = constructor_Int();"
  , "  newInt->val = this->val + other->val;"
  , "  return newInt;"
  , "}"
  , "struct obj_Int* method_Int_TIMES(struct obj_Int* this, struct obj_Int* other) {"
  , "  struct obj_Int* newInt = constructor_Int();"
  , "  newInt->val = this->val * other->val;"
  , "  return newInt;"
  , "}"
  , "struct obj_String* constructor_String() {"
  , "  struct obj_String* this = malloc(sizeof(struct obj_String));"
  , "  this->clazz = (void*)&class_String;"
  , "  return this;"
  , "}"
  , "struct obj_Bool* method_String_EQUALS(struct obj_String* this, struct obj_Obj* other) {"
  , "  struct obj_Bool* eq = constructor_Bool();"
  , "  if (other->clazz == (void*)&class_String) {"
  , "    eq->val = (strcmp(this->data, ((struct obj_String*)other)->data) == 0);"
  , "  } else {"
  , "    eq->val = 0;"
  , "  }"
  , "  return eq;"
  , "}"
  , "struct obj_String* method_String_STR(struct obj_String* this) {"
  , "  return this;"
  , "}"
  , "struct obj_String* method_String_PLUS(struct obj_String* this, struct obj_String* other) {"
  , "  struct obj_String* newString = constructor_String();"
  , "  int length1 = strlen(this->data);"
  , "  int length2 = strlen(other->data);"
  , "  newString->data = malloc((length1 + length2 + 1) * sizeof(char));"
  , "  strcpy(newString->data, this->data);"
  , "  strcpy((newString->data) + length1, other->data);"
  , "  return newString;"
  , "}"
  , "struct obj_Nothing* constructor_Nothing() {"
  , "  struct obj_Nothing* this = malloc(sizeof(struct obj_Nothing));"
  , "  this->clazz = (void*)&class_Nothing;"
  , "  return this;"
  , "}"
  , "struct obj_Bool* method_Nothing_EQUALS(struct obj_Nothing* this, struct obj_Obj* other) {"
  , "  struct obj_Bool* eq = constructor_Bool();"
  , "  eq->val = ((struct obj_Nothing*)other == var_none);"
  , "  return eq;"
  , "}"
  , "struct obj_String* method_Nothing_STR(struct obj_Nothing* this) {"
  , "  struct obj_String* str = constructor_String();"
  , "  str->data = malloc(5 * sizeof(char));"
  , "  strcpy(str->data, \"NONE\");"
  , "  return str;"
  , "}"
  ]
