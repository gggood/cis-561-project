{
module Parser where
import Scanner
}

%name calc
%tokentype { Token }
%error { parseError }

%token
    class            { Class }
    def              { Def }
    extends          { Extends }
    if               { If }
    elif             { Elif }
    else             { Else }
    while            { While }
    return           { Return }
    typecase         { Typecase }
    and              { And }
    or               { Or }
    not              { Not }
    '('              { LeftParen }
    ')'              { RightParen }
    '{'              { LeftBracket }
    '}'              { RightBracket }
    ','              { Comma }
    ';'              { Semicolon }
    '.'              { Period }
    ':'              { Colon }
    '+'              { Plus }
    '-'              { Minus }
    '*'              { Mul }
    '/'              { Div }
    '='              { Assignment }
    eq               { EQUAL }
    atleast          { ATLEAST }
    less             { LESS }
    atmost           { ATMOST }
    more             { MORE }
    number           { NumberToken $$ }
    id               { Identifier $$ }
    string           { StringLiteral $$ }
    eof              { EOF }
    err              { ERROR }

%%

Program : Classes Statements { Program $1 $2 }

Classes : {- empty -}                    { [] }
        | Classes Class                  { $2 : $1 }

Class : ClassSignature ClassBody         { $1 (fst $2) (snd $2) }

ClassSignature : class id '(' FormalArgs ')'               { ClassDef $2 $4 "Obj" }
               | class id '(' FormalArgs ')' extends id    { ClassDef $2 $4 $7 }

FormalArgs : {- Empty -}                 { [] }
           | FormalArgTail               { $1 }

FormalArgTail : FormalArgTail ',' id ':' id  { FormalArg $3 $5 : $1 }
              | id ':' id                    { [FormalArg $1 $3] }

ClassBody : '{' Statements Methods '}'   { ($2, $3) }

Methods : {- empty -}                            { [] }
        | Methods MethodSig '{' Statements '}'   { $2 $4 : $1 }

MethodSig : def id '(' FormalArgs ')'            { MethodDef $2 $4 "Nothing" }
          | def id '(' FormalArgs ')' ':' id     { MethodDef $2 $4 $7 }

Statements : {- empty -}                 { [] }
           | Statements Statement        { $2 : $1 }

Statement : LExp '=' RExp ';'            { Assign (fst $1) (snd $1) "$Bot" $3 }
          | LExp ':' id '=' RExp ';'     { Assign (fst $1) (snd $1) $3 $5 }
          | JumpStatement                { $1 }
          | IfBlock                      { $1 }
          | WhileBlock                   { $1 }
          | RExp ';'                     { Expression $1 }
          | TypeCase                     { $1 }

IfBlock : if RExp '{' Statements '}' IfTail { IfBlock $2 $4 $6 }

IfTail : {- empty -}                         { [] }
       | elif RExp '{' Statements '}' IfTail { [IfBlock $2 $4 $6] }
       | else '{' Statements '}'             { $3 }

WhileBlock : while RExp '{' Statements '}'   { WhileBlock $2 $4 }

JumpStatement : return ';'               { ReturnStatement (Var "none") }
              | return RExp ';'          { ReturnStatement $2 }

TypeCase : typecase RExp '{' TypeAlts '}'{ TypeCase $2 $4 }

TypeAlts : {- empty -}                   { [] }
         | TypeAlts TypeAlt              { $2 : $1 }

TypeAlt : id ':' id '{' Statements '}'   { TypeAlt $1 $3 $5 }

LExp : id                                { (Nothing, $1) }
     | Invocation '.' id                 { (Just $1, $3) }

Value : id                               { Var $1 }
      | number                           { IntLiteral $1 }
      | string                           { StrLiteral $1 }
      | '(' RExp ')'                     { $2 }

RealArgs : {- empty -}                   { [] }
         | TailArgs                      { $1 }

TailArgs : RExp                          { [$1] }
         | TailArgs ',' RExp             { $3 : $1 }

Invocation : Invocation '.' id '(' RealArgs ')'  { MethodCall $1 $3 $5 }
           | id '(' RealArgs ')'                 { Constructor $1 $3 }
           | Invocation '.' id                   { Access $1 $3 }
           | Value                               { $1 }

NotExpr : not NotExpr                    { NotExp $2 }
        | Invocation                     { $1 }

Logical : Logical and NotExpr            { AndExp $1 $3 }
        | Logical or NotExpr             { OrExp $1 $3 }
        | NotExpr                        { $1 }

Comparison : Comparison eq Logical       { MethodCall $1 "EQUALS" [$3] }
           | Comparison atmost Logical   { MethodCall $1 "ATMOST" [$3] }
           | Comparison less Logical     { MethodCall $1 "LESS" [$3] }
           | Comparison atleast Logical  { MethodCall $1 "ATLEAST" [$3] }
           | Comparison more Logical     { MethodCall $1 "MORE" [$3] }
           | Logical                     { $1 }

Negation : '-' Negation                  { MethodCall (IntLiteral 0) "MINUS" [$2] }
         | Comparison                    { $1 }

MultExp : MultExp '*' Negation           { MethodCall $1 "TIMES" [$3] }
        | MultExp '/' Negation           { MethodCall $1 "DIVIDE" [$3] }
        | Negation                       { $1 }

PlusExp : PlusExp '+' MultExp            { MethodCall $1 "PLUS" [$3] }
        | PlusExp '-' MultExp            { MethodCall $1 "MINUS" [$3] }
        | MultExp                        { $1 }

RExp : PlusExp                           { $1 }
{
parseError :: [Token] -> a
parseError tokens = error $ "Parse error on the token at: " ++ show tokens

data Program = Program [ClassDef] [Statement]
  deriving (Eq, Show)

data ClassDef = ClassDef { c_name :: String,
			   c_args :: [FormalArg],
			   c_parent :: String,
			   c_body :: [Statement],
			   c_methods ::[MethodDef] }
  deriving (Eq, Show)

data FormalArg = FormalArg { formalArgName :: String,
			     formalArgType :: String }
  deriving (Eq, Show)

data MethodDef = MethodDef { name :: String,
			     args :: [FormalArg],
			     returnType :: String,
			     body :: [Statement] }
  deriving (Eq, Show)

data Statement = Assign (Maybe Expr) String String Expr
               | IfBlock Expr [Statement] [Statement]
               | WhileBlock Expr [Statement]
               | ReturnStatement Expr
               | Expression Expr
               | TypeCase Expr [TypeAlt]
  deriving (Eq, Show)

data TypeAlt = TypeAlt String String [Statement]
  deriving (Eq, Show)

data Expr = Var String
          | Access Expr String
          | IntLiteral Int
          | StrLiteral String
          | MethodCall Expr String [Expr]
          | Constructor String [Expr]
          | AndExp Expr Expr
          | OrExp Expr Expr
          | NotExp Expr
  deriving (Eq, Show)

}
