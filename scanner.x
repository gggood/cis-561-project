{
module Scanner where
}

%wrapper "monadUserState"

$digit = 0-9      --digits
$alpha = [a-zA-Z]  --letters
$alphanum = [0-9a-zA-Z] -- digits and letters

$not_asterisk = [.\n] # \*
$not_slash = [.\n] # \/
$not_quote = [ . \n ] # \" --everything except quotes (even newline)
$in_string = [^\\\"]

$escape = [0btnrf\"\\]
$blank = [\t\n\r\ ]

@identifer = [$alpha \_] [$alphanum \_]*
@smallstr = ( $in_string | (\\ $escape) )+

tokens :-

  <0>$blank+                          { skip }
  <0>\/\/.*                           { skip }
  
  <0>\/\*                             { begin comment }
  <comment> $not_slash+               { skip }
  <comment> $not_asterisk ^ \/        { skip } -- a slash preceded by something other than an asterisk ("something" already consumed)
  <comment> \* ^ \/                   { begin 0 } -- a slash preceded by an asterisk (asterisk already consumed)

  <0>class                            { str_to_token (const Class) }
  <0>def                              { str_to_token (const Def) }
  <0>extends                          { str_to_token (const Extends) }
  <0>if                               { str_to_token (const If) }
  <0>elif                             { str_to_token (const Elif) }
  <0>else                             { str_to_token (const Else) }
  <0>while                            { str_to_token (const While) }
  <0>return                           { str_to_token (const Return) }
  <0>typecase                         { str_to_token (const Typecase) }
  <0>and                              { str_to_token (const And) }
  <0>or                               { str_to_token (const Or) }
  <0>not                              { str_to_token (const Not) }

  --Xun said I probably could have done all of these with one token taking a character argument
  --at first I thought the parser wouldn't be able to see inside the tokens but he's probably right
  <0>\(                               { str_to_token (const LeftParen) }
  <0>\)                               { str_to_token (const RightParen) }
  <0>\{                               { str_to_token (const LeftBracket) }
  <0>\}                               { str_to_token (const RightBracket) }
  <0>\,                               { str_to_token (const Comma) }
  <0>\;                               { str_to_token (const Semicolon) }
  <0>\.                               { str_to_token (const Period) }
  <0>\:                               { str_to_token (const Colon) }
  <0>\=                               { str_to_token (const Assignment) }
  
  <0>\+                               { str_to_token (const Plus) }
  <0>\-                               { str_to_token (const Minus) }
  <0>\*                               { str_to_token (const Mul) }
  <0>\/                               { str_to_token (const Div) }
  <0>\=\=                             { str_to_token (const EQUAL) }
  <0>\<\=                             { str_to_token (const ATMOST) }
  <0>\<                               { str_to_token (const LESS) }
  <0>\>\=                             { str_to_token (const ATLEAST) }
  <0>\>                               { str_to_token (const MORE) }
  <0>$digit+                          { str_to_token (NumberToken . read) }
  <0>[$alpha\_] [$alphanum\_]*        { str_to_token Identifier }

  <0> \"                              { begin smallstring }
  <smallstring> @smallstr             { str_to_token StringLiteral }
  <smallstring> \"                    { begin 0 }
  <smallstring> \n                    { throw_error (const "String literals cannot be multiple lines.") `andBegin` 0 }
  <smallstring> \\(. # $escape)       { throw_error (++ " is an invalid escape code.") }
  
 -- removes the 3 quotes from the string literal; not sure how to do this in a better way
 -- unfortunately in Haskell removing 3 from the end of a list is slower than reversing twice
 -- and removing 3 from the start
  <0> \"\"\"                          { begin string }
  <0> \"\"\"\"\"\"                    { str_to_token (const $ StringLiteral "") }
  <string> (\"?\"?$not_quote)+ \"\"\" { let remove n list = iterate tail list !! n
                                        in str_to_token (StringLiteral . reverse . (remove 3) . reverse ) `andBegin` 0 }
{

data Token = Class |
             Def |
             Extends |
             If |
             Elif |
             Else |
             While |
             Return |
             Typecase |
             LeftParen |
             RightParen |
             LeftBracket |
             RightBracket |
             Comma |
             Semicolon |
             Period |
             Colon |
             Plus |
             Minus |
             Mul |
             Div |
             Assignment |
             EQUAL |
             ATMOST |
             LESS |
             ATLEAST |
             MORE |
             And |
             Or |
             Not |
             NumberToken Int |
             Identifier String |
             StringLiteral String |
             EOF |
             ERROR
             deriving (Eq, Show)

-- the lexer will run until it finds more than 10 errors
max_errors :: Int
max_errors = 10

-- uses a function supplied by code fragments above to convert a string
-- (the string matched by the regex corresponding with that code fragment)
-- into a token
str_to_token :: (String -> Token) -> AlexAction Token
str_to_token f (_, _, _, str) length = return $ f (take length str)

-- as above, but instead throwing an error
throw_error :: (String -> String) -> AlexAction Token
throw_error f (pos, _, _, str) len = do
  error_list <- get_error_state
  if length error_list >= 10
    then alexError $ error_merge $ reverse error_list
    else do
        set_error_state $ error_preface (f str) : error_list
        return Plus
  where error_preface e = "Error at " ++ show_pos pos ++ ": " ++ e

-- the custom user state is a list of errors to be saved and displayed later
type AlexUserState = [String]

alexInitUserState = []

get_error_state :: Alex AlexUserState
get_error_state = Alex $ \s -> Right (s, alex_ust s)

set_error_state :: AlexUserState -> Alex ()
set_error_state ss = Alex $ \s -> Right (s{alex_ust = ss}, ())

alexEOF :: Alex Token
alexEOF = do
  errors <- get_error_state
  code <- alexGetStartCode
  if code /= 0
    then alexError "Ended inside a string or comment. Missing a closing \" or /*"
    else if errors == []
      then return EOF
      else alexError $ error_merge errors

primary_alex :: Alex [Token]
primary_alex = do
  t <- alexMonadScan
  if t == EOF then return []
              else (t:) <$> primary_alex

to_tokens :: String -> Either String [Token]
to_tokens input = runAlex input primary_alex

show_pos :: AlexPosn -> String
show_pos (AlexPn _ line col) = "Line " ++ show line ++ ", Col " ++ show col

error_merge :: [String] -> String
error_merge = foldr (\e acc -> e ++ ('\n':acc)) [] . reverse

add_error :: Token -> String -> Alex Token
add_error t e = do
  error_list <- get_error_state
  if length error_list >= max_errors
    then alexError $ error_merge (e:error_list)
    else do set_error_state (e:error_list)
            return t

}
