module Main where

import System.Environment

import Compile
import Scanner
import Parser
import TypeCheck

main :: IO ()
main = do
  inFile <- head <$> getArgs
  outFile <- head <$> tail <$> getArgs
  text <- readFile inFile
  lexedTokens <- return $ to_tokens text
  program <- return $ either error calc lexedTokens
  writeFile outFile $ unlines $ compile program $ typeCheck $ program

